/*
@Time : 2018/6/12 下午5:23
@Author : tengjufeng
@File : string
@Software: GoLand
*/

package main

import (
	"fmt"
	"github.com/bitly/go-simplejson"
	"strconv"
	"strings"
)

func main() {
	//判断str字符串是否包含另一个字符串 return bool
	fmt.Println("判断str字符串是否包含另一个字符串:", strings.Contains("abcd", "a"))

	//数组转换成字符串
	str := []string{"abc", "def", "hig"}
	fmt.Println("数组转换成字符串:", strings.Join(str, "@"))
	//字符串转数组
	fmt.Println("字符串转数组", strings.Split("abc@def@hig", "@"))

	//在字符串s中查找另一个字符串的位置，找不到返回-1
	fmt.Println("在字符串s中查找另一个字符串的位置，找不到返回-1:", strings.Index("abcde", "de"))

	//重复字符串n次
	fmt.Println("重复字符串n次:", strings.Repeat("ha", 3))

	//字符串替换 n替换次数，小于零全部替换
	fmt.Println("字符串替换 n替换次数，小于零全部替换:", strings.Replace("hahahacccddd", "ha", "ee", -1))

	//去除字符串头部和尾部的指定字符串
	fmt.Println("去除字符串头部和尾部的指定字符串:", strings.Trim("hahadefhaha", "ha"))

	//
	fmt.Println(strconv.FormatInt(1234, 10))
	fmt.Println(strconv.ParseInt("1234", 10, 64))

	jsons := `{"servers":[{"serverName":"Shanghai_VPN","serverIP":"127.0.0.1"},{"serverName":"Beijing_VPN","serverIP":"127.0.0.2"}]}`
	jsona, _ := simplejson.NewJson([]byte(jsons))
	arr, _ := jsona.Get("servers").Array()
	for k, _ := range arr {
		ma := jsona.Get("servers").GetIndex(k).MustMap()
		for key, value := range ma {
			fmt.Println(key, value)
		}
	}

}
